
# Changelog

All notable changes to this project will be documented in this file.

## 2023-03-24

- ADDED terraform to multiple containers due to broader usage in AWS ecosystem. See README.md for affected containers.
- ADDED git to support terraform
- ADDED Nano to support local editing

